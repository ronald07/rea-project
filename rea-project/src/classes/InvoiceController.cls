public with sharing class InvoiceController {
	public List<Invoice> invoices { get; private set; }
	
	public InvoiceController() {
		invoices = getInvoices();
	}
	
    public List<Invoice> getInvoices(){
	  	HttpRequest req = new HttpRequest();

	  	//Set HTTPRequest Method
	  	req.setMethod('GET');
	  	req.setHeader('Connection','keep-alive');
		req.setEndpoint('https://sf01.corp.realestate.com.au');
	
  		Http http = new Http();
   
		HTTPResponse res = http.send(req);
        
        List<Invoice> invoices = parse(res.getBody());       
        return invoices;                
	}
	
	public List<Invoice> parse(String body) {
		List<Invoice> invoices = (List<Invoice>)JSON.deserialize(body, List<Invoice>.class);
		return invoices;
	}
	
	public class Invoice {
		public Integer company_id { get; private set; }
		Datetime invoice_date;
		Datetime due_date;
		public Decimal total { get; private set; }
		Decimal amount_due;
		
		public Invoice(Integer company_id, Datetime invoiceDate, Datetime due_date, Decimal total, Decimal amount_due) {
			this.company_id = company_id;
			this.invoice_date = invoice_date;
			this.due_date = due_date;
			this.total = total;
			this.amount_due = amount_due;
		}
	}
}