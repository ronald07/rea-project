<apex:component controller="VanaHCM.TypeAheadRecordSelectorController"> 
	<apex:attribute name="loadJquery" description="If jqueryCore need to load" type="boolean" required="false"  default="true"/>
	
	
	<apex:attribute name="dataSourceQuery" description="List of record which need to be display in list" type="String" required="true" assignTo="{!dataSourceQuery}"/>
	<apex:attribute name="renderLabel" description="If a label should be display along with the select list." type="boolean" required="false"  default="true" />
	<apex:attribute name="listLabel" description="If a label should be display along with the select list." type="string" required="false"  default="Select/Enter Worker Name" />
	
	<apex:attribute name="selectedRecord" description="" type="string" required="false"  assignTo="{!selectedRecord_id}"/>
	
	<apex:attribute name="onChangeHandler" description="JS function name which will be called on worker change event" type="string" required="true"  default="onWorkerChange" />
	
	<apex:styleSheet value="{!URLFOR($Resource.VanaHCM__ResourceBundle,'development-bundle/themes/base/jquery.ui.all.css')}" />
	<apex:variable value="" var="v1" rendered="{!loadJquery}">
    	<script src="{!URLFOR($Resource.ResourceBundle,'js/jquery-1.5.1.js')}" ></script>
    </apex:variable>
    <script src="{!URLFOR($Resource.ResourceBundle,'development-bundle/ui/jquery.ui.core.js')}"></script>
    <script src="{!URLFOR($Resource.ResourceBundle,'development-bundle/ui/jquery.ui.widget.js')}" />
    <script src="{!URLFOR($Resource.ResourceBundle,'development-bundle/ui/jquery.ui.button.js')}" />
    <script src="{!URLFOR($Resource.ResourceBundle,'development-bundle/ui/jquery.ui.position.js')}" />
    <script src="{!URLFOR($Resource.ResourceBundle,'development-bundle/ui/jquery.ui.autocomplete.js')}" />
    
    <style>
        .ui-button { margin-left: -1px; } 
        .ui-button-icon-only .ui-button-text { padding: 0.35em; } 
        .ui-autocomplete-input { margin: 0; padding: 0.48em 0 0.47em 0.45em;width:230px; }
        .ui-widget-content {
            border: 1px solid #79B4CD;
         }
         body .x-btn-pressed, body .x-btn-click, body .x-btn-menu-active, body button:active, body .btn:active, body .btnCancel:active, body .menuButton .menuButtonButton:active {
            border-color: #79B4CD;
         }
         button.ui-button-icon-only {
             background: none repeat scroll 0 0 #FCFCFC;
             border: 1px solid #79B4CD;
         }
         
         .ui-autocomplete {
			max-height: 350px;
			overflow-y: auto;
			/* prevent horizontal scrollbar */
			overflow-x: hidden;
		}
    </style>
    
    
    <apex:outputPanel rendered="{!renderLabel}">
         <div style="padding-top:8px;">
             <apex:outputLabel value="{!listLabel}"></apex:outputLabel>
         </div>  
     </apex:outputPanel> 
     <apex:outputPanel >
         <select id="workerList" data-role="none">
          <apex:repeat value="{!recordSelectOptions}" var="main">
          <apex:repeat value="{!main}" var="v">
                 <apex:variable var="v1" value="" rendered="{!v.Value = selectedRecord_id}">
                    <option value="{!v.Value}" selected="selected">{!v.Label}</option>
                 </apex:variable>
                 <apex:variable var="v2" value="" rendered="{!NOT(v.Value = selectedRecord_id)}">
                    <option value="{!v.Value}" >{!v.Label}</option>
                 </apex:variable>  
             </apex:repeat>
             </apex:repeat>
        </select>       
    </apex:outputPanel> 
    
    
    <script>
  	$.noConflict();
    (function( $ ) {
        $.widget( "ui.combobox", {
            _create: function() {
                var self = this,
                    select = this.element.hide(),
                    selected = select.children( ":selected" ),
                    value = selected.val() ? selected.text() : "";
                var input = this.input = $( "<input>" )
                    .insertAfter( select )
                    .val( value )
                    .autocomplete({
                        delay: 0,
                        minLength: 0,
                        source: function( request, response ) {
                            var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
                            response( select.children( "option" ).map(function() {
                                var text = $( this ).text();
                                if ( this.value && ( !request.term || matcher.test(text) ) )
                                    return {
                                        label: text.replace(
                                            new RegExp(
                                                "(?![^&;]+;)(?!<[^<>]*)(" +
                                                $.ui.autocomplete.escapeRegex(request.term) +
                                                ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                            ), "<strong>$1</strong>" ),
                                        value: text,
                                        option: this
                                    };
                            }) );
                        },
                        select: function( event, ui ) {
                            ui.item.option.selected = true;
                            self._trigger( "selected", event, {
                                item: ui.item.option
                            });
                            {!onChangeHandler}(String(ui.item.option.value));
                            
                        },
                        change: function( event, ui ) {
                            if ( !ui.item ) {
                                var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
                                    valid = false;
                                select.children( "option" ).each(function() {
                                    if ( $( this ).text().match( matcher ) ) {
                                        this.selected = valid = true;
                                        return false;
                                    }
                                });
                                if ( !valid ) {
                                    // remove invalid value, as it didn't match anything
                                    $( this ).val( "" );
                                    select.val( "" );
                                    input.data( "autocomplete" ).term = "";
                                    return false;
                                }
                            }
                        }
                    })
                    .addClass( "ui-widget ui-widget-content ui-corner-left" );

                input.data( "autocomplete" )._renderItem = function( ul, item ) {
                    return $( "<li></li>" )
                        .data( "item.autocomplete", item )
                        .append( "<a>" + item.label + "</a>" )
                        .appendTo( ul ); 
                }; 

                this.button = $( '<button type="button">&nbsp;</button>' )
                    .attr( "tabIndex", -1 )
                    .attr( "title", "Show All Items" )
                    .insertAfter( input )
                    .button({
                        icons: {
                            primary: "ui-icon-triangle-1-s"
                        },
                        text: false 
                    }) 
                    .removeClass( "ui-corner-all" )
                    .addClass( "ui-corner-right ui-button-icon" )
                    .click(function() {
                        // close if already visible
                        if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
                            input.autocomplete( "close" );
                            return;
                        }

                        // work around a bug (likely same cause as #5265)
                        $( this ).blur();

                        // pass empty string as value to search for, displaying all results
                        input.autocomplete( "search", "" );
                        input.focus();
                    });
            },

            destroy: function() {
                this.input.remove();
                this.button.remove();
                this.element.show();
                $.Widget.prototype.destroy.call( this );
            }
        });
    })( jQuery );
    
    jQuery(document).ready(function($) {
    	var selId;
    	$('#workerList').combobox();
        selId = "{!selectedRecord_id}";
        if(selId != ""){
        	$("#workerList").find("option").each(function(){
		    if($(this).val() != '-1'){
		        var strOption =  $(this).val();
		        strOption = strOption.substring(0, strOption.length -3);
		        if(strOption == selId){
		            $('#workerList').next().val($(this).text());
		
		        }
		    }
		
			});
        }
    });
    </script>
<!-- To use -->
<!-- 
	<c:WorkerSelector onChangeHandler="onRecordSelected" selectedRecord="{!selectedRecordID}" renderLabel="true" listLabel="Worker " loadJquery="false"/>
	<c:TypeAheadRecordSelector onChangeHandler="onRecordSelected" selectedRecord="{!selectedRecordID}" renderLabel="false" loadJquery="true" dataSourceQuery="Select id, name, Performance_Assignment__c, Default_Plan__c, Active__c From Goal_Plan__c where Default_Plan__c = true"/>
	<script>
		function onRecordSelected(rid){
			
		}
	</script>
 -->    
</apex:component>