<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>false</enableActivities>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>Benefit_Plan__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Benefit Plan</label>
        <referenceTo>Benefit_Plan__c</referenceTo>
        <relationshipName>Benefit_Rate_Type</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Coverage_Code__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Used for Health Plan Types</inlineHelpText>
        <label>Coverage Code</label>
        <picklist>
            <picklistValues>
                <fullName>Employee Only</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Employee + Spouse</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Employee + Dependents</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Family</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Domestic Partner</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Covered Person</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Coverage_Maximun__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Coverage Maximun</label>
        <precision>17</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Coverage_Rate_Type__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Benefit Rate Type</label>
        <picklist>
            <picklistValues>
                <fullName>Flat Amount</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Age Graded</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Percent of Base</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Length of Service</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Coverage_Rate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Coverage Level</label>
        <picklist>
            <picklistValues>
                <fullName>Flat Amount</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>2X Base</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>2X Base + Flat Amount</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Base + Flat Amount</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Deduction_Codes__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Deduction Code</label>
        <referenceTo>Deduction_Codes__c</referenceTo>
        <relationshipName>Coverage_Rate</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Effective_Date_Benefit_Rate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Effective Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Field1__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Rate or Percent</label>
        <picklist>
            <picklistValues>
                <fullName>Rate</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Percent</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Premium_Frequency__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Premium Frequency</label>
        <picklist>
            <picklistValues>
                <fullName>Monthly</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Annually</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Pay Period</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Rate_per_Unite__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Rate per Unit Coverage</label>
        <picklist>
            <picklistValues>
                <fullName>100</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>1000</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>2000</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>2500</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Rounding__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Rounding</label>
        <picklist>
            <picklistValues>
                <fullName>None</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Up to Nearest 1000</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Down to Nearest 1000</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Up to Nearest 100</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Down to Nearest 100</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <label>Benefit Rates</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Coverage Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Benefit Rates</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Benefit_Plan__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Benefit_Plan__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Benefit_Plan__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchResultsAdditionalFields>Benefit_Plan__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
