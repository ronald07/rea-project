<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>true</enableActivities>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>Absence_Reason__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Absence Reason</label>
        <referenceTo>Absence_Reason__c</referenceTo>
        <relationshipLabel>Absence Requests</relationshipLabel>
        <relationshipName>Absence_Requests</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Absence_Type_Request__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Absence Type</label>
        <referenceTo>Absence_Types__c</referenceTo>
        <relationshipName>Absence_Request</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Actual_Absence__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Absence Requested</label>
        <precision>6</precision>
        <required>false</required>
        <scale>1</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Approved__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Approved</label>
        <picklist>
            <picklistValues>
                <fullName>Yes</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>No</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Department_Manager__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Department Manager</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Absence_Requests</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Department__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Department</label>
        <referenceTo>Department__c</referenceTo>
        <relationshipLabel>Absence Requests</relationshipLabel>
        <relationshipName>Absence_Requests</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Departments__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Departments List</label>
        <picklist>
            <picklistValues>
                <fullName>Department 1</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Department 2</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>MultiselectPicklist</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Dump_End_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>this field will be used to show the date on UI only</description>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Dump_Start_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>this field will be used to show the date on UI only</description>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Employee_Full_Name__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>$User.FirstName&amp;&quot; &quot;&amp; $User.LastName</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Employee Full Name</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Employee_ID__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>$User.Id</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Employee ID</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>End_date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Half_Days__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Half Days</label>
        <picklist>
            <picklistValues>
                <fullName>All Days</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Last Day Only</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>First Day Only</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>First and Last Day</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Hours__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Hours</label>
        <precision>17</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Manager_Name__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Manager Name</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Absence_Request</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Notes__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Notes</label>
        <length>32768</length>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Requested_By__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Worker</label>
        <referenceTo>Worker__c</referenceTo>
        <relationshipLabel>Absence Requests</relationshipLabel>
        <relationshipName>Time_Off_Requests</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Start_date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <type>DateTime</type>
    </fields>
    <label>Absence Request</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Absence_Type_Request__c</columns>
        <columns>Employee_ID__c</columns>
        <columns>Employee_Full_Name__c</columns>
        <columns>Start_date__c</columns>
        <columns>End_date__c</columns>
        <columns>Manager_Name__c</columns>
        <columns>Approved__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>Request ID-{00000000}</displayFormat>
        <label>Absence Request ID</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Absence Requests</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Requested_By__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Absence_Type_Request__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Start_date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>End_date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Actual_Absence__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Approved__c</customTabListAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
    </searchLayouts>
    <sharingModel>Private</sharingModel>
    <sharingReasons>
        <fullName>Manager_Share__c</fullName>
        <label>Manager Share</label>
    </sharingReasons>
</CustomObject>
