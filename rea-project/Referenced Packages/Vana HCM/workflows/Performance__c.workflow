<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Final_Rating_Completed</fullName>
        <description>Final Rating Completed</description>
        <protected>false</protected>
        <recipients>
            <field>Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Worker_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Vana_HCM/Final_Rating_Completed</template>
    </alerts>
    <alerts>
        <fullName>Manager_Review_Complete</fullName>
        <description>Manager Review Complete</description>
        <protected>false</protected>
        <recipients>
            <field>Worker_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Vana_HCM/Manager_Review_Completed</template>
    </alerts>
    <alerts>
        <fullName>New_Performance_Created</fullName>
        <description>New Performance Created</description>
        <protected>false</protected>
        <recipients>
            <field>Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Vana_HCM/Performance_Initiated</template>
    </alerts>
    <alerts>
        <fullName>Notify_Employee</fullName>
        <description>Notify Employee</description>
        <protected>false</protected>
        <recipients>
            <field>Worker_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Vana_HCM/Employee_Notification</template>
    </alerts>
    <alerts>
        <fullName>Self_Review_Complete</fullName>
        <description>Self Review Complete</description>
        <protected>false</protected>
        <recipients>
            <field>Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Vana_HCM/Employee_Self_Review_Completed</template>
    </alerts>
    <fieldUpdates>
        <fullName>Final_Approval</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Final Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Performance_Manager_Email</fullName>
        <field>Manager_Email__c</field>
        <formula>Worker__r.User__r.Manager.Email</formula>
        <name>Performance Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Performance_Worker_Email</fullName>
        <field>Worker_Email__c</field>
        <formula>Worker__r.Email_Address_Worker__c</formula>
        <name>Performance Worker Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Plan</fullName>
        <field>Name</field>
        <formula>Performance_Type__r.Name  +  &quot; for &quot; +  &quot; &quot; +  Worker__r.Full_Name__c</formula>
        <name>Plan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Submitted</fullName>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Status Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Create Plan</fullName>
        <actions>
            <name>Plan</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OwnerId    != null</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Final Rating Completed</fullName>
        <actions>
            <name>Final_Rating_Completed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Overall_Rating_Name__c &lt;&gt; null</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Manager Review Complete</fullName>
        <actions>
            <name>Manager_Review_Complete</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Status__c, &quot;Manager Review Complete&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Employee</fullName>
        <actions>
            <name>Notify_Employee</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Status__c, &quot;Evaluation&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Performance Created</fullName>
        <actions>
            <name>New_Performance_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Performance__c.Manager__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Performance Email Update</fullName>
        <actions>
            <name>Performance_Manager_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Performance_Worker_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Performance__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Self Review Complete</fullName>
        <actions>
            <name>Self_Review_Complete</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Status__c, &quot;Self Review Complete&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
