<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>New_Candidate</fullName>
        <field>Name</field>
        <formula>First_Name_Candidate__c + &quot; &quot; +  Last_Name_Candidate__c</formula>
        <name>New Candidate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Candidate Name Change</fullName>
        <actions>
            <name>New_Candidate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Last_Name_Candidate__c  )  ||   ISCHANGED(   First_Name_Candidate__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Create Candidate</fullName>
        <actions>
            <name>New_Candidate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Last_Name_Candidate__c  &lt;&gt; null</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
