<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>New_Compensation</fullName>
        <field>Name</field>
        <formula>Compensation_ID__c</formula>
        <name>New Compensation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>New Compensation</fullName>
        <actions>
            <name>New_Compensation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Worker_Compensation__c  != null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
