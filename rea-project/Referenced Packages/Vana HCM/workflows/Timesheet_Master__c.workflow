<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Approval_Action</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approval Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Inital_Submit_Action</fullName>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Inital Submit Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejection_Action</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Rejection Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
