<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_to_Recruiting_team_Approved</fullName>
        <description>Email Alert to Recruiting team Approved</description>
        <protected>false</protected>
        <recipients>
            <recipient>koula.moutsos@rea-group.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Vana_HCM/ApprovedMail</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_Recruiting_team_Rejection</fullName>
        <description>Email Alert to Recruiting team Rejection</description>
        <protected>false</protected>
        <recipients>
            <recipient>koula.moutsos@rea-group.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Vana_HCM/RejectionMail</template>
    </alerts>
</Workflow>
