<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>List</customSettingsType>
    <customSettingsVisibility>Public</customSettingsVisibility>
    <description>Custom setting to store Vana HCM related configs.</description>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>Activate_Delegated_Approvals__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked it will active the sharing of the Worker data with the delegated approver.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked it will active the sharing of the Worker data with the delegated approver.</inlineHelpText>
        <label>Activate Delegated Approvals</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Apply_With_LinkedIn_Key__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Apply With LinkedIn Key</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Attach_resume_only_Job_Application_UI__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>By default when candidate upload resume on job application page it also populate various field details, to disable that check this true.</description>
        <externalId>false</externalId>
        <inlineHelpText>By default when candidate upload resume on job application page it also populate various field details, to disable that check this true.</inlineHelpText>
        <label>Attach resume only (Job Application UI)</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Candidate_Email_Is_Required__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, candidate record should have email in order to save.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, candidate record should have email in order to save.</inlineHelpText>
        <label>Candidate Email Is Required</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Cascade_Goals__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Cascade Goals</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Check_Date_in_Self_Service__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked it would produce an error message on the About Me Address, Phone, and Name changes if the Effective Date chosen was prior to the current date.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked it would produce an error message on the About Me Address, Phone, and Name changes if the Effective Date chosen was prior to the current date.</inlineHelpText>
        <label>Check Date in Self Service</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Create_Contact_from_Candidate__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked system will create a contact on new candidate record creation.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked system will create a contact on new candidate record creation.</inlineHelpText>
        <label>Create Contact from Candidate</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Display_Absence_Status_Current__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked &quot;Balance (Current)&quot; and  &quot;Taken (Current)&quot; fields would be displayed on Request Time Off UI</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked &quot;Balance (Current)&quot; and  &quot;Taken (Current)&quot; fields would be displayed on Request Time Off UI</inlineHelpText>
        <label>Display Absence Status (Current)</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Display_Absence_Status_Pay_Period_Ending__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked &quot;Balance (Pay Period Ending)&quot; and &quot;Taken (Pay Period Ending)&quot; fields would be displayed on Request Time Off UI</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked &quot;Balance (Pay Period Ending)&quot; and &quot;Taken (Pay Period Ending)&quot; fields would be displayed on Request Time Off UI</inlineHelpText>
        <label>Display Absence Status (Pay Period End)</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Display_Goal_Category__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked,  this would display the Goal Category field on My Goals Page and Performance Plan page.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked,  this would display the Goal Category field on My Goals Page and Performance Plan page.</inlineHelpText>
        <label>Display Goal Category</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Display_Weight__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, this would display the Weight field on My Goals Page</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, this would display the Weight field on My Goals Page</inlineHelpText>
        <label>Display Weight</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>EmailAuthor_Additional_To_Field_ID__c</fullName>
        <defaultValue>&apos;p24&apos;</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>EmailAuthor Additional To Field ID</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EmailAuthor_Related_To_Field_ID__c</fullName>
        <defaultValue>&apos;p3_lkid&apos;</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>EmailAuthor Related To Field ID</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Enable_Absence_Deletion__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked option to delete absence requests will be shown.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked option to delete absence requests will be shown.</inlineHelpText>
        <label>Enable Absence Deletion</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Enable_Allowance_On_Work_Detail_Page__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked Allowance section will be displayed on Work Detail page.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked Allowance section will be displayed on Work Detail page.</inlineHelpText>
        <label>Enable Allowance On Work Detail Page</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Enable_Approval_Process_Manager_Check__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>enable/disable manager check &amp; error display &quot;Manager does not exist&quot; on EmployeeAbsenseRequest page</description>
        <externalId>false</externalId>
        <label>Enable Approval Process Manager Check</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Enable_Careers_Job_Function_Search__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If Checked a drop down search for Job Function will be displayed on job listing page.</description>
        <externalId>false</externalId>
        <inlineHelpText>If Checked a drop down search for Job Function will be displayed on job listing page.</inlineHelpText>
        <label>Enable Careers Job Function Search</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Enable_Careers_Location_Search__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If Checked a drop down search for Job Location will be displayed on job listing page.</description>
        <externalId>false</externalId>
        <inlineHelpText>If Checked a drop down search for Job Location will be displayed on job listing page.</inlineHelpText>
        <label>Enable Careers Location Search</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Enable_Careers_Region_Search__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Enable Careers Region Search</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Enable_Careers_Search__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If Checked a type ahead search for job title will be displayed on job listing page.</description>
        <externalId>false</externalId>
        <inlineHelpText>If Checked a type ahead search for job title will be displayed on job listing page.</inlineHelpText>
        <label>Enable Careers Search</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Enable_Compensation_On_Work_Detail_Page__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked compensation section will be displayed on Work Detail page.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked compensation section will be displayed on Work Detail page.</inlineHelpText>
        <label>Enable Compensation On Work Detail Page</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Enable_Contact_Button_On_My_Team_Page__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Enable Contact Button On My Team Page</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Enable_Google_Distance_Calculation__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Enable Google Distance Calculation On Job Application while new job application insert/update</description>
        <externalId>false</externalId>
        <inlineHelpText>Enable Google Distance Calculation On Job Application while new job application insert/update</inlineHelpText>
        <label>Enable Candidate Distance Calculation</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Enable_Onboarding_Process__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, it will activate the onboarding process. In which onboarding tasks will created.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, it will activate the onboarding process. In which onboarding tasks will created.</inlineHelpText>
        <label>Enable Onboarding Process</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Enable_Person_Sharing__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Enable Person Sharing</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Enable_Team_Button_On_My_Team_Page__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked Team button will be displayed on My Team page.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked Team button will be displayed on My Team page.</inlineHelpText>
        <label>Enable Team Button On My Team Page</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Enable_Variable_Compensation_Work_Detail__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked Variable Compensation section will be displayed on Work Detail page.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked Variable Compensation section will be displayed on Work Detail page.</inlineHelpText>
        <label>Enable Variable Compensation Work Detail</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Enable_WA_Position_Ajax__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>on my team promotion/transfer click on the new page whether the position ajax call is allowed (No Allowed) to re render data on form</description>
        <externalId>false</externalId>
        <label>Enable Work Assignment Position Ajax</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Enable_Work_Assignment_Compensation__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>will be used on my team promotion to show/hide compensation on the page</description>
        <externalId>false</externalId>
        <label>Enable Work Assignment Compensation</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Enable_Work_Button_On_My_Team_Page__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, work button would be displayed on My Team page. The button is linked to detail about the worker&apos;s assignment and compensation.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, work button would be displayed on My Team page. The button is linked to detail about the worker&apos;s assignment and compensation.</inlineHelpText>
        <label>Enable Work Button On My Team Page</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Force_Internal_CRUD_and_FLS_Checks__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked application will check object and related fields permission for the logged in user.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked application will check object and related fields permission for the logged in user.</inlineHelpText>
        <label>Force Internal CRUD and FLS Checks</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Google_Map_Zoom_Level__c</fullName>
        <deprecated>false</deprecated>
        <description>Parameter to decide Zoom level for Job Requisition detail page</description>
        <externalId>false</externalId>
        <inlineHelpText>Parameter to decide Zoom level for Job Requisition detail page</inlineHelpText>
        <label>Google Map Zoom Level</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Hide_Chatter_Profile__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Hide Chatter Profile</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Hide_Rapid_Hire_Address__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Hide Rapid Hire Address</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Hide_Rapid_Hire_Compensation__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Hide Rapid Hire Compensation</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Hide_Rapid_Hire_Equity_Plan__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Hide Rapid Hire Equity Plan</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Hide_Rapid_Hire_National_ID__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Hide Rapid Hire National ID</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Hide_Rapid_Hire_Phone__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Hide Rapid Hire Phone</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Hide_Rapid_Hire_Work_Assignment__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Hide Rapid Hire Work Assignment</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Last_Worker_ID_Used__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Last Worker ID Used</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Linked_In_Company_Id__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>LinkedIn Company Id</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Notes_Attachment_About_Me__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Notes &amp; Attachment About Me</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Root_Worker__c</fullName>
        <deprecated>false</deprecated>
        <description>The ID of worker who is top level of worker Hierarchy.</description>
        <externalId>false</externalId>
        <inlineHelpText>The ID of worker who is top level of worker Hierarchy.</inlineHelpText>
        <label>Root Worker</label>
        <length>18</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Set_Worker_Manager_From_Work_Assignment__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>this will be used to update the worker-user.manager field at the time of setting worker.manager from work assignment.manager</description>
        <externalId>false</externalId>
        <label>Set Worker Manager From Work Assignment</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Share_Absence_Request_with_Manager__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Share Absence Request with Manager</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Share_Objective_With_Manager__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Share Objective With Manager</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Share_Performance_With_Manager__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Share Performance With Manager</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Share_Person_Hierarchy__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If this is true and person sharing settings are private/public read only then person will be shared with entire manager hierarchy.</description>
        <externalId>false</externalId>
        <inlineHelpText>If this is true and person sharing settings are private/public read only then person will be shared with entire manager hierarchy.</inlineHelpText>
        <label>Share Person Hierarchy</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Share_Worker_with_Managers_Manager__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Share Worker with Managers&apos; Manager</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Absence_In_Salesforce_Calendar__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked approved absence requests would be displayed in Salesforce Calendar for the user.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked approved absence requests would be displayed in Salesforce Calendar for the user.</inlineHelpText>
        <label>Show Absence In Salesforce Calendar</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Absence_Link_On_ESS__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Absence Link On ESS</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Absence__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Will be used to show/hide absence button on my team page</description>
        <externalId>false</externalId>
        <label>Show Absence</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Allowance_On_ESS__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked Allowance section will be displayed on on About Me page (ESS).</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked Allowance section will be displayed on About Me page (ESS).</inlineHelpText>
        <label>Show Allowance On ESS</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Calendar_Link_On_ESS__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Calendar Link On ESS</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Clock_Link_On_ESS__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Clock Link On ESS</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Compensation_On_ESS__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked Compensation history section will be displayed on About Me page (ESS).</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked Compensation history section will be displayed on About Me page (ESS).</inlineHelpText>
        <label>Show Compensation On ESS</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Compensation_on_Work_Assignment__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Compensation on Work Assignment</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Contact_Details_on_Mobile_Org_Chart__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, will show the email, work phone, and mobile phone from the user profile for the users.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, will show the email, work phone, and mobile phone from the user profile for the users.</inlineHelpText>
        <label>Show Contact Details on Mobile Org Chart</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Goals_Link_On_ESS__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Goals Link On ESS</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Holidays_on_Absence_Calendar__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Holidays on Absence Calendar</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Multi_Rater__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Multi-Rater</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Org_Chart_Link_On_ESS__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Org Chart Link On ESS</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Performance_Link_On_ESS__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Performance Link On ESS</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Promotion__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Promotion</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Social_Sharing__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Social Sharing</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Termination__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Termination</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Timesheet_Link_On_ESS__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Timesheet Link On ESS</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Transfer__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Transfer</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Variable_Compensation_On_ESS__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked Variable Compensation section will be displayed on About Me page (ESS).</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked Variable Compensation section will be displayed on About Me page (ESS).</inlineHelpText>
        <label>Show Variable Compensation On ESS</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Work_Link_On_ESS__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Profile Link On ESS</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Work_On_ESS__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked current assignment and work history section will be displayed on About Me page (ESS).</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked current assignment and work history section will be displayed on About Me page (ESS).</inlineHelpText>
        <label>Show Work On ESS</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Worker_Certification_on_ES__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Worker Certification on ESS</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Worker_Education_on_ESS__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Worker Education on ESS</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Worker_Languages_on_ESS__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Worker Languages on ESS</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Worker_License_on_ESS__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Worker License on ESS</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Worker_Passport_on_ESS__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Worker Passport on ESS</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Worker_Visa_on_ESS__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Show Worker Visa on ESS</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Thanks_Message__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Thanks_Message</label>
        <length>200</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Total_Compensation_Year__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Compensation Year</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Update_User_Delegated_Approver__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked it will determine if the Delegate user identified on the Work Assignment will update the Delegated Approver field on the User record.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked it will determine if the Delegate user identified on the Work Assignment will update the Delegated Approver field on the User record.</inlineHelpText>
        <label>Update User Delegated Approver</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Update_User_Managers_Manager__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked HCM will update worker&apos;s managers manager field from worker&apos;s manager&apos;s manager field. [Using worker assignment]</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked HCM will update worker&apos;s managers manager field from worker&apos;s manager&apos;s manager field. [Using worker assignment]</inlineHelpText>
        <label>Update User Managers Manager</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Update_User_Name_Details_From_Worker__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked system will update worker user record&apos;s firstname and lastname with related worker&apos;s firstname and lastname.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked system will update worker user record&apos;s firstname and lastname with related worker&apos;s firstname and lastname.</inlineHelpText>
        <label>Update User Name Details From Worker</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Update_User_Title_From_Worker_Assignment__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked system will update worker user record&apos;s title with related worker&apos;s current worker assignment.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked system will update worker user record&apos;s title with related worker&apos;s current worker assignment.</inlineHelpText>
        <label>Update User Title From Worker Assignment</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Use_Nickname_If_Available_In_Worker_Name__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked and worker have nickname filled, Worker name would be updated with Nickname + Lastname else it would be Firstname + Lastname</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked and worker have nickname filled, Worker name would be updated with Nickname + Lastname else it would be Firstname + Lastname</inlineHelpText>
        <label>Use Nickname If Available In Worker Name</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Validate_Existing_Person__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, A search will be performed for existing person records by the fields National ID, Personal Email, and Full Name when saving a worker__c (Person) record. A warning message will be displayed before saving.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, A search will be performed for existing person records by the fields National ID, Personal Email, and Full Name when saving a worker__c (Person) record. A warning message will be displayed before saving.</inlineHelpText>
        <label>Validate Existing Person</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Validate_Position_Department__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Check on My team page which enable the Department &amp; Position Department</description>
        <externalId>false</externalId>
        <label>Validate Position Department</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>careersite__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Career Website URL</label>
        <length>200</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>linkedpostmessage__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>LinkedIn Post Message</label>
        <length>200</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rsaas_CountryKey__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>rsaas CountryKey</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rsaas_SubUserId__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>rsaas SubUserId</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rsaas_UserKey__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>rsaas UserKey</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Vana HCM Settings</label>
</CustomObject>
