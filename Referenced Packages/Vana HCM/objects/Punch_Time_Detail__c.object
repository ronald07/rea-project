<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>true</enableActivities>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>Client__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Client</label>
        <referenceTo>Client__c</referenceTo>
        <relationshipLabel>Punch Time Details</relationshipLabel>
        <relationshipName>Punch_Time_Details</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Earnings_Code__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Earnings Code</label>
        <referenceTo>Earnings_Code__c</referenceTo>
        <relationshipLabel>Punch Time Details</relationshipLabel>
        <relationshipName>Punch_Time_Details</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Initiatives__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Initiatives</label>
        <referenceTo>Initiatives__c</referenceTo>
        <relationshipLabel>Punch Time Details</relationshipLabel>
        <relationshipName>Punch_Time_Details</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Location__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Location</label>
        <referenceTo>Location__c</referenceTo>
        <relationshipLabel>Punch Time Details</relationshipLabel>
        <relationshipName>Punch_Time_Details</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Position__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Position</label>
        <referenceTo>Position__c</referenceTo>
        <relationshipLabel>Punch Time Details</relationshipLabel>
        <relationshipName>Punch_Time_Details</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Project__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Project</label>
        <referenceTo>Project__c</referenceTo>
        <relationshipLabel>Punch Time Details</relationshipLabel>
        <relationshipName>Punch_Time_Details</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Punch_Date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Punch Date</label>
        <required>true</required>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Punch_Duration__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(Punch_Out_Time__c-Punch_In_Time__c &gt; 0 ,(
ROUND((((Punch_Out_Time__c-Punch_In_Time__c ) * 1440) / 60), 2)), 0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Punch Duration</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Punch_In_Time__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Punch In Time</label>
        <required>true</required>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Punch_Out_Time__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Punch Out Time</label>
        <required>false</required>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Rate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Rate</label>
        <precision>6</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Timesheet__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Timesheet</label>
        <referenceTo>Timesheet_Master__c</referenceTo>
        <relationshipLabel>Punch Time Details</relationshipLabel>
        <relationshipName>Punch_Time_Details</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Total_Amount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(AND(NOT(ISNULL(Rate__c)),NOT(ISNULL(Punch_Duration__c))), (Rate__c *  Punch_Duration__c ), 0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Worker__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Worker</label>
        <referenceTo>Worker__c</referenceTo>
        <relationshipLabel>Punch Time Details</relationshipLabel>
        <relationshipName>Punch_Time_Details</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <label>Punch Time Detail</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Worker__c</columns>
        <columns>Punch_Date__c</columns>
        <columns>Earnings_Code__c</columns>
        <columns>Punch_In_Time__c</columns>
        <columns>Punch_Out_Time__c</columns>
        <columns>Punch_Duration__c</columns>
        <columns>Timesheet__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <label>Punch Time Detail</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Punch Time Details</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
