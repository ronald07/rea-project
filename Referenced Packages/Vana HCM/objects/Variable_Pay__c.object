<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>true</enableActivities>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>Adjustment_Percentage__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Adjustment %</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Adjustment__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Adjustment Amount</label>
        <precision>17</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Award_Amount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Award Amount</label>
        <precision>17</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Award_Date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Award Date</label>
        <required>false</required>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Current_Variable_Pay__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Current Variable Pay</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Effective_Date_VP__c</fullName>
        <defaultValue>TODAY()</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Effective Date</label>
        <required>false</required>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Event_Varilable_Pay__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Variable Pay Event</label>
        <referenceTo>Compensation_Event__c</referenceTo>
        <relationshipLabel>Variable Pay</relationshipLabel>
        <relationshipName>Variable_Pay</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Guaranteed_Amount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Guaranteed Amount</label>
        <precision>17</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Guaranteed__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Guaranteed %</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Payout_Period__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payout Period</label>
        <length>100</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Target_Amount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Target Amount</label>
        <precision>17</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Target__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Target %</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Variable_Pay_Plan__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Variable Pay Plan</label>
        <referenceTo>Compensation_Plan__c</referenceTo>
        <relationshipLabel>Variable Pay</relationshipLabel>
        <relationshipName>Variable_Pay</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Worker_ID_Variable__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>TEXT(Worker_Variable__r.Worker_External_ID__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Worker ID</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Worker_Variable__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Worker</label>
        <referenceTo>Worker__c</referenceTo>
        <relationshipLabel>Variable Pay</relationshipLabel>
        <relationshipName>Variable_Pay</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <label>Variable Pay</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>VP-{0}</displayFormat>
        <label>Variable Pay Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Variable Pay</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Worker_Variable__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Variable_Pay_Plan__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Event_Varilable_Pay__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Effective_Date_VP__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Current_Variable_Pay__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Target__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Target_Amount__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Award_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Award_Amount__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Worker_Variable__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Variable_Pay_Plan__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Event_Varilable_Pay__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Effective_Date_VP__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Current_Variable_Pay__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Target__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Target_Amount__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Award_Date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Award_Amount__c</lookupDialogsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
