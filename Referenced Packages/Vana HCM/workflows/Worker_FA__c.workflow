<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Worker_Termination</fullName>
        <description>Worker Termination</description>
        <protected>false</protected>
        <recipients>
            <recipient>koula.moutsos@rea-group.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Vana_HCM/MSS_Termination</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Status</fullName>
        <field>Record_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_A</fullName>
        <field>Record_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Submit</fullName>
        <field>Record_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
