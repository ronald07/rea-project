<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>New_Dependent</fullName>
        <field>Name</field>
        <formula>First_Name_Dep_Ben__c  + &quot; &quot; +  Last_Name_Dep_Ben__c</formula>
        <name>New Dependent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>New Dep Ben</fullName>
        <actions>
            <name>New_Dependent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OwnerId   &lt;&gt; null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
