<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>New_Question</fullName>
        <field>Name</field>
        <formula>TEXT(VALUE( Questions_Performance_Plan__r.Name))</formula>
        <name>New Question</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>New Question</fullName>
        <actions>
            <name>New_Question</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Name == null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
