<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>New_Worker_Comptency</fullName>
        <field>Name</field>
        <formula>Competency__r.Name</formula>
        <name>New Worker Comptency</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>New Worker Comptency</fullName>
        <actions>
            <name>New_Worker_Comptency</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Competency__c  &lt;&gt; null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
