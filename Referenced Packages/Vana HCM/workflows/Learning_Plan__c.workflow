<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Learning_Plan</fullName>
        <description>New Learning Plan</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Vana_HCM/Learning_Plan_Created</template>
    </alerts>
    <rules>
        <fullName>New Learning Plan</fullName>
        <actions>
            <name>New_Learning_Plan</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Worker_LearningPlan__c  &lt;&gt; null</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
