<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Job_Posted</fullName>
        <description>New Job Posted</description>
        <protected>false</protected>
        <recipients>
            <field>Hiring_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>REA_Recruitment/New_Job_Requisition_old</template>
    </alerts>
    <fieldUpdates>
        <fullName>Hiring_Manager_Email_Update</fullName>
        <field>Hiring_Manager_Email__c</field>
        <formula>Hiring_Manager__r.Email_Address_Worker__c</formula>
        <name>Hiring Manager Email Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Interviewer_Email_Update</fullName>
        <field>Interviewer_Email__c</field>
        <formula>Interviewer__r.Email_Address_Worker__c</formula>
        <name>Interviewer Email Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recruiter_Email_Update</fullName>
        <field>Recruiter_Email__c</field>
        <formula>Recruiter__r.Email_Address_Worker__c</formula>
        <name>Recruiter Email Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Job Opening Email Update</fullName>
        <actions>
            <name>Hiring_Manager_Email_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Interviewer_Email_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Recruiter_Email_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Job_Opening__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Job Opening Notification</fullName>
        <actions>
            <name>New_Job_Posted</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Job_Opening__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
