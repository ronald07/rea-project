<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>New_Succession_Candidate</fullName>
        <field>Name</field>
        <formula>&quot;Succession Candidate:&quot; + &quot; &quot; +    Worker_Talent__r.Full_Name__c</formula>
        <name>New Succession Candidate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>New Succession Candidate</fullName>
        <actions>
            <name>New_Succession_Candidate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Succession_Plan_Talent__c  &lt;&gt; null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
