<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>New_Address</fullName>
        <field>Name</field>
        <formula>TEXT(Address_Type__c)</formula>
        <name>New Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>New Address</fullName>
        <actions>
            <name>New_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISPICKVAL(Address_Type__c, &apos;&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
