<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Person_Name_Change</fullName>
        <description>Person Name Change</description>
        <protected>false</protected>
        <recipients>
            <recipient>koula.moutsos@rea-group.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Vana_HCM/ApprovedMail</template>
    </alerts>
    <fieldUpdates>
        <fullName>New_Worker</fullName>
        <field>Name</field>
        <formula>First_Name__c  +  &quot; &quot; +  Last_Name__c</formula>
        <name>New Worker</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Create New Hire</fullName>
        <active>true</active>
        <formula>First_Name__c  != null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IT Notification of New Hire</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Worker__c.Worker_ID__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Name Change</fullName>
        <actions>
            <name>Person_Name_Change</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Name Change</description>
        <formula>ISCHANGED( Last_Name__c )  ||   ISCHANGED(  First_Name__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
