<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Benefit_Program_Enrollment</fullName>
        <field>Name</field>
        <formula>Benefit_Program__r.Name</formula>
        <name>Benefit Program Enrollment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Benefit Program Enrollment</fullName>
        <actions>
            <name>Benefit_Program_Enrollment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Benefit_Program__c  &lt;&gt; null</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
