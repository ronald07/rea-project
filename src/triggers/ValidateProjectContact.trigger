/*added some comments*/
/*author : Ronald*/
trigger ValidateProjectContact on Project_ContactDEMO__c (before insert) {
	Set<ID> NewProjectID= new Set<ID>();
	Set<ID>ProjectContactID= new Set<ID>();
	Set<ID>AccountIDs= new Set<ID>();
	Set<ID>AcctContactID= new Set<ID>();
	Set<ID>accountBillID= new Set<ID>();
	Set<ID>contactBillID= new Set<ID>();
	map<ID, Project_ContactDEMO__c> prjContactMap= new map<ID, Project_ContactDEMO__c>();
	if(trigger.isBefore){
		for(Project_ContactDEMO__c pc:trigger.new){
			NewProjectID.add(pc.New_Media_Project__c);
			ProjectContactID.add(pc.Contact__c);
			prjContactMap.put(pc.Contact__c,pc);
		}
		for(Project_Account__c pa:[SELECT Account__c,Account_Role__c
									from Project_Account__c 
									WHERE New_Media_Project__c IN : NewProjectID  ]){
			AccountIDs.add(pa.Account__c);
			if(pa.Account_Role__c=='Billing Account'){
				accountBillID.add(pa.Account__c);
			}
		}
		for(Contact con:[SELECT ID,AccountID from Contact where AccountID IN : AccountIDs]){
			AcctContactID.add(con.ID);
			if(accountBillID.contains(con.AccountID)){
				contactBillID.add(con.ID);
			}
		}
		if(!AcctContactID.containsAll(ProjectContactID)){
				trigger.new[0].addError('Cannot add this contact.Contact not belongs to any related account of the project.');
		}else{
			for(ID conID:ProjectContactID){
					if(prjContactMap.get(conID).Contact_Role__c=='Bill To Contact' && (contactBillID.isEmpty() || !contactBillID.contains(conID))){
						trigger.new[0].addError('You are trying to add a bill to contact without a billing account or the contact is not part of the billing account.');
					}
			
			}
		
		}
		
	
	}

}