trigger UpdateJobRequisitionApprover on VanaHCM__Job_Opening__c (before insert, before update) {
/* 03/06/2013 - Ver 1.0 - Koula Moutsos
                          Before insert, before update
                          Retrieve the Cost Centre Approver and update the Job Requisition.
                          Only required when the Job Requisition Status is equal to new. 
   17/06/2013 - Ver 1.1 - Andrew Manetakis (Cloud Sherpas)
                          Refactor to use maps and only 1 loop
   17/06/2013 - Ver 1.2 - Koula Moutsos
                          Set the Cost Centre Approver to null if the cost centre is not set.*/
    
    //pull out only the cost centres associated with the given job openings
    List<ID> costCentreIds = new List<ID>();
    for (VanaHCM__Job_Opening__c jobOpening : Trigger.new){
        costCentreIds.add(jobOpening.VanaHCM__Cost_Center_Job_Opening__c);
    }
    
    //build a map of all the valid cost centres
    Map<id,VanaHCM__Cost_Centre__c> costCentreMap = new Map<id,VanaHCM__Cost_Centre__c>([SELECT id, name, VanaHCM__Effective_Date_CostCenter__c, REA_Cost_Centre_Approver__c 
                                                                                         FROM   VanaHCM__Cost_Centre__c 
                                                                                         WHERE  id IN: costCentreIds
                                                                                         AND    REA_Cost_Centre_Active__c = True
                                                                                         AND    VanaHCM__Effective_Date_CostCenter__c <= today]);
    
    //iterate through all job openings to check if status = new & cost centre is populated
    for (VanaHCM__Job_Opening__c jobOpening : Trigger.new)
    {    
        if(jobOpening.VanaHCM__Job_Opening_Status__c == 'New')
        {
            if(jobOpening.VanaHCM__Cost_Center_Job_Opening__c!=null)        
            {
                //get the matching cost centre from our map and assign the approver of it to our job opening
                VanaHCM__Cost_Centre__c cc = costCentreMap.get(jobOpening.VanaHCM__Cost_Center_Job_Opening__c);
                if(cc!=null) 
                {
                    jobOpening.REA_Job_Requisition_Approver__c = cc.REA_Cost_Centre_Approver__c;  
                }
            }    
            else
            { 
                jobOpening.REA_Job_Requisition_Approver__c = null;
            }    
        }
    }  
}