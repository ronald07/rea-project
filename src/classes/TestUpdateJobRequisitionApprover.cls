@isTest
public class TestUpdateJobRequisitionApprover{
/* 17/06/2013 - Ver 1.0 -   Andrew Manetakis
                            Test for the UpdateJobRequisitionApprover Trigger
   25/06/2013 - Ver 1.1 -   Koula Moutsos
                            Added VanaHCM__Number_of_Openings__c = 1 &
                            VanaHCM__State_Province__c to the VanaHCM__Job_Opening__c makeJobOpening method
                                                   */
    
    public static testMethod void testJobReqAppNewStatus() {
         
        //create a cost centre
        VanaHCM__Cost_Centre__c costCentre = new VanaHCM__Cost_Centre__c();
        costCentre.REA_Cost_Centre_Active__c = true;
        costCentre.VanaHCM__Effective_Date_CostCenter__c = System.today() - 1;
        costCentre.REA_Cost_Centre_Approver__c = UserInfo.getUserId();
        insert costCentre;
        
        //create a position
        VanaHCM__Position__c position = new VanaHCM__Position__c(VanaHCM__Position_Title__c = 'Worker');
        insert position;
        
        //create a manager
        VanaHCM__Worker__c manager = new VanaHCM__Worker__c(VanaHCM__First_Name__c = 'Bob');
        insert manager;
        
        Test.startTest();
        
        //create the job openings to test for
        List<VanaHCM__Job_Opening__c> jobOpenings = new List<VanaHCM__Job_Opening__c>();
        for(integer i=0; i<20; i++) {
            VanaHCM__Job_Opening__c jo = makeJobOpening(costCentre, position, manager);
            jo.VanaHCM__Job_Opening_Status__c = 'New';
            jobOpenings.add(jo);
        } 
        insert jobOpenings;
        
        Test.stopTest();
        
        List<VanaHCM__Job_Opening__c> jobOpeningsCheck = [SELECT REA_Job_Requisition_Approver__c 
                                                          FROM VanaHCM__Job_Opening__c
                                                          WHERE id IN: jobOpenings];
        for(VanaHCM__Job_Opening__c jo : jobOpeningsCheck) {
            System.assertEquals(costCentre.REA_Cost_Centre_Approver__c, jo.REA_Job_Requisition_Approver__c);
        } 
    }
    public static testMethod void testJobReqAppNotNewStatus() {
        
        //create a cost centre
        VanaHCM__Cost_Centre__c costCentre = new VanaHCM__Cost_Centre__c();
        costCentre.REA_Cost_Centre_Active__c = true;
        costCentre.VanaHCM__Effective_Date_CostCenter__c = System.today() - 1;
        costCentre.REA_Cost_Centre_Approver__c = UserInfo.getUserId();
        insert costCentre;
        
        //create a position
        VanaHCM__Position__c position = new VanaHCM__Position__c(VanaHCM__Position_Title__c = 'Worker');
        insert position;
        
        //create a manager
        VanaHCM__Worker__c manager = new VanaHCM__Worker__c(VanaHCM__First_Name__c = 'Bob');
        insert manager;
        
        Test.startTest();
        
        //create the job openings to test for
        List<VanaHCM__Job_Opening__c> jobOpenings = new List<VanaHCM__Job_Opening__c>();
        for(integer i=0; i<20; i++) {
            VanaHCM__Job_Opening__c jo = makeJobOpening(costCentre, position, manager);
            jo.VanaHCM__Job_Opening_Status__c = 'Existing';
            jobOpenings.add(jo);
        } 
        insert jobOpenings;
        
        Test.stopTest();
        
        List<VanaHCM__Job_Opening__c> jobOpeningsCheck = [SELECT REA_Job_Requisition_Approver__c 
                                                          FROM VanaHCM__Job_Opening__c
                                                          WHERE id IN: jobOpenings];
        for(VanaHCM__Job_Opening__c jo : jobOpeningsCheck) {
            System.assertEquals(null, jo.REA_Job_Requisition_Approver__c);
        } 
       
    }
    
    
     
    private static VanaHCM__Job_Opening__c makeJobOpening(VanaHCM__Cost_Centre__c costCentre, VanaHCM__Position__c position, VanaHCM__Worker__c manager) {
        VanaHCM__Job_Opening__c jo = new VanaHCM__Job_Opening__c();
        jo.VanaHCM__Job_Opening_Status__c = 'New';
        jo.VanaHCM__Number_of_Openings__c = 1;
        jo.VanaHCM__Cost_Center_Job_Opening__c = costCentre.id;
        jo.VanaHCM__Target_Start_Date__c = System.today() + 1;
        jo.VanaHCM__Salary_From__c = 100;
        jo.VanaHCM__Salary_To__c = 110;
        jo.VanaHCM__Salary_is_Per__c = 'Annum';
        jo.VanaHCM__Currency__c = 'AUD';
        jo.VanaHCM__Bonus_Eligible__c = 'No';
        jo.VanaHCM__Position__c = position.id;
        jo.VanaHCM__Hiring_Request__c = 'New';
        jo.VanaHCM__Full_Part_Time_Job__c = 'Full Time';
        jo.VanaHCM__Regular_Temporary__c = 'Regular';
        jo.VanaHCM__Hiring_Manager__c = manager.id;
        jo.VanaHCM__CIty_Town__c = 'Sydney';
        jo.VanaHCM__State_Province__c = 'NSW';
        return jo;
    }
        
}